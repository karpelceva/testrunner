var Like = React.createClass({
    render: function() {
        return (
            <div className="like clearfix">
                {this.props.username}
            </div>
            );
    }
});

var LikeList = React.createClass({

    render: function() {

        var likeList = this.props.postLikes.map(function(like){
            return (
                <Like username={like.username}/>
                );
        });

        return (
            <div style={{display: this.props.display}} className="likes">
                {likeList}
            </div>
            );
    }
});

var Comment = React.createClass({
    render: function() {
        return (
            <div className="comment clearfix">
            <img className="comment__picture" alt="" src={this.props.commentData.picture}/>
            <div className="comment__comment-info">
                <div className="comment__username">{this.props.commentData.username} <span className="pointer" title={this.props.commentData.date}>says</span>:</div>
                <div className="comment__text">{this.props.commentData.text}</div>
            </div>
            </div>
            );
    }
});
var CommentList = React.createClass({

    render: function() {
        var commentList = this.props.postComments.map(function(comment){
            return (
                <Comment commentData={comment}/>
                );
        });

        return (
            <div style={{display: this.props.display}} className="comments">
            {commentList}
            </div>
            );
    }
});

var Friend = React.createClass({
    render: function() {
        var comma = (!this.props.last)?", ":"";
        return (
            <a href="#">
                {this.props.name}{comma}
            </a>
            );
    }
});

var Mood = React.createClass({
    render: function() {
        var friendCount = this.props.withWho.length,
        friends = this.props.withWho.map(function(friend, i){
            return (
                <Friend last={i==friendCount-1} name={friend.username}/>
                );
        });

        return (
            <div className="post__mood-where">Feeling {this.props.moodType} with {friends} </div>
            );
    }
});

var Post = React.createClass({

    getInitialState: function(){
        return {
            displayComments: 'none',
            displayLikes: 'none',
            liked: false
        }
    },

    handleLikeDisplay: function(){
        if(this.state.displayLikes == 'none') {
            this.setState({
                displayComments: 'none',
                displayLikes: 'block'
            });
            return;
        }

        this.setState({
            displayLikes : 'none'
        });

    },

    handleCommentDisplay: function(){
        if(this.state.displayComments == 'none')
        {
            this.setState({
                displayComments: 'block',
                displayLikes: 'none'
            });
            return;
        }

        this.setState({
            displayComments : 'none'
        });
    },

    handleLike: function(){
        if(!this.state.liked)
        {
            this.setState({
                liked:true // Yay +1 like
            });

            this.props.postData.likes.push({
                username: 'Alinute'
            });
        }
    },

    render: function() {

        var likeStyle = 'post__heart pointer js-heart ';
        if(this.state.liked)
        {
            likeStyle += 'post__heart_deactivated';
        }

        return (
            <article className="post js-post clearfix">
                <div className="post__user-info">
                    <img className="post__picture" src={this.props.postData.picture} alt="" />
                </div>

                <div className="post__post-info">
                <div className="post__username">{this.props.postData.username} <span className="pointer" title={this.props.postData.date}>says</span>:</div>
                <div className="post__text">{this.props.postData.text}</div>

                <Mood moodType={this.props.postData.mood} withWho={this.props.postData.withWho}/>

                <div className="post__location">Near {this.props.postData.where}</div>

                <div onClick={this.handleCommentDisplay} className="post__comments-link pointer js-comment">Comments ({this.props.postData.comments.length})</div>
                <div onClick={this.handleLikeDisplay} className="post__likes-link pointer js-like">Likes ({this.props.postData.likes.length})</div>
                <img src="img/like.png" onClick={this.handleLike} className={likeStyle} />

                <CommentList display={this.state.displayComments} postComments={this.props.postData.comments} />
                <LikeList display={this.state.displayLikes} postLikes={this.props.postData.likes} />

                </div>
            </article>
            );
}

});

var PostList = React.createClass({
    render: function() {
        var postList = this.props.data.map(function(post){
            return (
                <Post postData={post}/>
                );
        });

        return (
            <main className="posts js-posts">
                {postList}
            </main>
            );
    }
});


var data = generate(2);

React.render(
    <PostList data={data}/>,
    document.getElementById('posts')
    );