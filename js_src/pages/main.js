require(['jsonGenerator'], function(jsonGenerator) {

    var totalTime = 0,
        testModule = null,
        results = [],
        $results = $('.js-results'),
        $info = $('.js-info-text'),
        $save = $('.js-save'),
        $download = $('.js-download'),
        url = 'index.php';

    //save results
    $('body').on('click', '.js-save', function() {
        $.ajax({
            type: 'POST',
            url: url,
            data: {
                'results': $results.html()
            },
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
        });
    });

    //download results
    $('body').on('click', '.js-download', function() {
        $results.html('');
        $.ajax({
            type: 'GET',
            url: url+'?results',
            dataType: 'html',
            success: function(data) {
                $results.html(data);
            }
        });
    });


    testModule = window.testModule;
    testModule.init();
    if (testModule.getModuleName() == 'Angular') {
        angular.bootstrap(document.getElementsByClassName('app'), ['socialApp']);
    }

    if ($download.data('exists') == '1') { //if there are results on the server already, show possibility to download them
        $download.removeClass('hide');
    }

    $('.js-run').on('click', function() {
        $(this).prop('disabled', true);
        $info.html('Tests are running...');
        $results.html('');
        numbersOfPosts = parseValues($('.js-number-of-posts').val());
        numberOfTests = parseValues($('.js-number-of-tests').val())[0];


        if ($('#renderTest:checked').length) {
            runRenderTest(testModule);
        }
        if ($('#likeTest:checked').length) {
            runLikeTest(testModule);
        }
        if ($('#addTest:checked').length) {
            runAddTest(testModule);
        }
        if ($('#likeAllTest:checked').length) {
            runLikeAllTest(testModule);
        }

        $save.removeClass('hide');

        $info.html('');
        $(this).prop('disabled', false);

    });


    function printResults(moduleName, type, numberOfPosts) {
        var res = Math.round(totalTime / numberOfTests * 1000) / 1000;
        console.log(res);
        $results
            .append("<hr/>")
            .append(moduleName + ' framework | '+type+' test for ' + numberOfPosts + ' elements: ' + res + '(average)')
            .append('<br > Time for each test, in seconds:')
            .append("<hr/>");
        for (var key in results) {
            if (results.hasOwnProperty(key)) {
                $results.append(results[key]+'<br>');
            }
        }
        results = [];
    }

    // render all elements
    function runRenderTest(testModule) {
        for (key in numbersOfPosts) {
            if (numbersOfPosts.hasOwnProperty(key)) {
                var numberOfPosts = numbersOfPosts[key];
                totalTime = 0;
                for (var i = 1; i <= numberOfTests; i++) {
                    var data = jsonGenerator.generate(numberOfPosts);
                    testModule.setPosts(data);

                    start();
                    testModule.digest('first');
                    end();

                    testModule.clearFeed();
                    testModule.digest();
                }
                printResults(testModule.getModuleName(), 'Render a list', numberOfPosts);
            }
        }
    }

    //change one value of ONE post in a set of already rendered elements
    function runLikeTest(testModule) {
        for (key in numbersOfPosts) {
            if (numbersOfPosts.hasOwnProperty(key)) {
                var numberOfPosts = numbersOfPosts[key];
                totalTime = 0;
                for (var i = 1; i <= numberOfTests; i++) {
                    var data = jsonGenerator.generate(numberOfPosts),
                        likedPost = Math.floor(Math.random() * (numberOfPosts - 1)),
                        moduleName = testModule.getModuleName();
                    testModule.setPosts(data);
                    testModule.digest();

                    if (moduleName == 'React' || moduleName == 'Knockout') {
                        start();
                        testModule.like(likedPost);
                        end();
                    } else {
                        testModule.like(likedPost);
                        start();
                        testModule.digest();
                        end();
                    }

                    testModule.clearFeed();
                    testModule.digest();
                }
                printResults(testModule.getModuleName(), 'Like one post', numberOfPosts);
            }
        }
    }


    //change one value in ALL POSTS in a set of already rendered elements
    function runLikeAllTest(testModule) {
        for (key in numbersOfPosts) {
            if (numbersOfPosts.hasOwnProperty(key)) {
                var numberOfPosts = numbersOfPosts[key];
                totalTime = 0;
                for (var i = 1; i <= numberOfTests; i++) {
                    var data = jsonGenerator.generate(numberOfPosts),
                        moduleName = testModule.getModuleName();
                    testModule.setPosts(data);
                    testModule.digest();

                    if (moduleName == 'Knockout') {
                        start();
                        testModule.likeAll(numbersOfPosts);
                        end();
                    } else {
                        testModule.likeAll(numbersOfPosts);
                        start();
                        testModule.digest();
                        end();
                    }

                    testModule.clearFeed();
                    testModule.digest();
                }
                printResults(testModule.getModuleName(), 'Like all posts', numberOfPosts);
            }
        }
    }

    //add one post to a set of already rendered elements
    function runAddTest(testModule) {
        for (key in numbersOfPosts) {
            if (numbersOfPosts.hasOwnProperty(key)) {
                var numberOfPosts = numbersOfPosts[key];
                totalTime = 0;
                for (var i = 1; i <= numberOfTests; i++) {
                    var data = jsonGenerator.generate(numberOfPosts),
                        newPost = jsonGenerator.generate(1);
                    testModule.setPosts(data);
                    testModule.digest();
                    testModule.addPost(newPost);
                    start();
                    testModule.digest();
                    end();
                    testModule.clearFeed();
                    testModule.digest();
                }
                printResults(testModule.getModuleName(), 'Add one post', numberOfPosts);
            }
        }
    }

    function start() {
        startTime = Date.now();
    }

    function end() {
        var endTime = (Date.now() - startTime) / 1000;
        totalTime += endTime;
        results.push(endTime);
    }

    function parseValues(values) {
        var usableValues = [];
        if (values !== '' && values.search('/^[0-9,\s+]+$/g')) {
            values = values.replace(/\s+/g, '').split(",");
            for (var index in values) {
                if (values[index] > 0) //Ok
                {
                    usableValues.push(values[index]);
                }
            }
        }
        return usableValues;
    }

});
