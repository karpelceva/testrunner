define([], function() {
    "use strict";

    Date.prototype.yyyymmdd = function() {
        var yyyy = this.getFullYear().toString();
        var mm = this.getMonth().toString();
        var dd = this.getDate().toString();
        var hh = this.getHours().toString();
        var m = this.getMinutes().toString();
        return yyyy +
            '.' + (mm[1] ? mm : "0" + mm[0]) +
            '.' + (dd[1] ? dd : "0" + dd[0]) +
            ' ' + (hh[1] ? hh : "0" + hh[0]) +
            ':' + (m[1] ? m : "0" + m[0]);
    };

    function rand(from, to) {
        return Math.floor(Math.random() * (to + 1)) + from;
    }

    function randText(numberOfWords, wordLength) {
        var text = "";
        var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        var wordLength = (wordLength === undefined) ? rand(2, 12) : wordLength;


        for (var i = 0; i < numberOfWords; i++) {
            for (var j = 0; j < wordLength; j++) {
                text += possible.charAt(Math.floor(Math.random() * possible.length));
            }
            if (numberOfWords > 1) {
                text += " ";
            }
        }

        return text;
    }

    function generateId() {
        return "_" + randText(1, 20);
    }

    function mood() {
        var moods = ["Excited", "Mellow", "Upset", "Comfortable", "Angry", "Annoyed", "Confused", "Dreamy", "Excited", "Guilty", "High", "Loved", "Peaceful", "Silly", "Relieved", "Relaxed", "Sad", "Satisfied", "Shoked", "Smart", "Stressed", "Surprised", "Sympathetic", "Lazy", "Lonely", "Mad", "Numb"];
        return moods[rand(0, moods.length - 1)];
    }

    function bool() {
        var bools = [false, true];
        return bools[rand(0, 1)];
    }

    function username() {
        var usernames = ["Anna", "Zack", "India", "Charlie", "Pitt", "Maria", "Stas", "Sasha", "Igor", "Marina", "Drew", "Peach", "Stacie", "Silly", "Micky", "Ginger", "Pat", "Gilly", "Pikachu", "Davina", "Klaus", "Mark", "Kim", "Don", "Rick", "Carol", "Rose", "Rosie", "Delta", "Lawana", "Tasia", "Kaila", "Hallie", "Myrna", "Bethanie", "Raguel", "Trudy", "Coleen", "Judy", "Takisha", "Krystal", "Ossie", "Lilly", "Dania"];
        return usernames[rand(0, usernames.length - 1)];
    }

    function comment() {
        return {
            '_id': generateId(),
            'picture': 'http://placehold.it/36x36',
            'username': username(),
            'date': generateDate(),
            'text': generateText()
        }
    }


    function withWho() {
        return {
            "_id": generateId(),
            "username": username()
        }
    }

    function likes() {
        return {
            "_id": generateId(),
            "username": username()
        }
    }

    function generateLikes() {
        var objArr = [];
        for (var i = 0; i < rand(2, 10); i++) {
            objArr.push(likes());
        }

        return objArr;
    }

    function generateDate() {
        var date = new Date(2013, 1, 1, 1, rand(1, 300000)); //no need for seconds
        return date.yyyymmdd();
    }

    function generateWithWho() {
        var objArr = [];
        for (var i = 0; i < 2; i++) {
            objArr.push(withWho());
        }

        return objArr;
    }

    function generateText() {
        return randText(rand(2, 10));
    }

    function address() {
        return rand(1, 9999) + " " + randText(5) + " " + rand(1, 9999);
    }

    function generateComments() {

        var objArr = [];

        for (var i = 0; i < rand(2, 7); i++) {
            objArr.push(comment());
        }

        return objArr;
    }

    function post() {
        return {
            "_id": generateId(),
            "isActive": bool(),
            "isEdited": bool(),
            "picture": "http://placehold.it/110x90",
            "date": generateDate(2014, 12, 12, 23),
            "text": generateText(),
            "username": username(),
            "where": address(),
            "mood": mood(),
            "withWho": generateWithWho(),
            "comments": generateComments(),
            "likes": generateLikes()
        }
    }

    return {
        generate: function(numberOfPosts) {
            var objArr = [];

            for (var i = 0; i < numberOfPosts; i++) {
                objArr.push(post());
            }

            return objArr;
        }
    }
});
