define(['emberTemplateCompiler', 'ember'], function(emberTemplateCompiler, ember) {
    "use strict";

    var self, app, postData = Ember.A();
    return {
        init: function() {
            self = this;
            app = Ember.Application.create();

            app.Router.map(function() {
                this.resource('home');
            });

            app.HomeRoute = Ember.Route.extend({
                model: function() {
                    return postData;
                }
            });
        },

        addPost: function(newPost) {
            postData.pushObject(newPost);
        },
        clearFeed: function() {
            // postData.removeObejcts();
        },
        digest: function(str) {
            var start = new Date();
            Ember.run.schedule('afterRender', this, function() {
                var end = new Date();
                console.log(str, end - start);
            });
        },
        getModuleName: function() {
            return "Ember";
        },
        like: function(likedPost) {
            postData.objectsAt([likedPost])[0].likes.push({
                    "_id": "5518298f90c066d2a7438442",
                    "username": "TSI user"
                });
        },
        likeAll: function(numberOfPosts) {},
        setPosts: function(data) {
             postData.pushObjects(data);
        }

    }
});
