define('knockoutTestModule',['knockout'], function(ko) {
    "use strict";
    var self,
        currentUser = {
            "_id": "5518298f90c066d2a7438442",
            "username": "TSI user"
        }

    function prepare(data) {
        var prepared = [];
        for (var i = 0; i < data.length; i++) {
            var post = data[i],
                comments = post.comments.map(function(comment){
                    return ko.observable(comment);
                }),
                likes = post.likes.map(function(like){
                    return ko.observable(like);
                });

            var newPost = {
                _id: ko.observable(post._id),
                comments: ko.observableArray(comments),
                date: ko.observable(post.date),
                isActive: ko.observable(post.isActive),
                isEdited: ko.observable(post.isEdited),
                likes: ko.observableArray(likes),
                mood: ko.observable(post.mood),
                picture: ko.observable(post.picture),
                text: ko.observable(post.text),
                username: ko.observable(post.username),
                where: ko.observable(post.where),
                withWho: ko.observable(post.withWho),
                showComments: ko.observable(false),
                showLikes: ko.observable(true)
            };

            prepared.push(newPost);
        }

        return prepared;
    }

    var observablePosts =  ko.observableArray();
    var preparedPosts = [];

    var PostModel = function () {

        this.posts = observablePosts;

        this.toggleComments = function(post) {
            post.showLikes(false);
            post.showComments(!post.showComments());
        }

        this.toggleLikes = function(post) {
            post.showComments(false);
            post.showLikes(!post.showLikes());
        }

        this.like = function(post) {
            var index = $('.js-post').index($('#'+post._id()));
            self.like(index);
        }

    }.bind(this);

    var postModelVar = null;

    return {
        init: function() {
            self = this;
            postModelVar = new PostModel();
            ko.applyBindings(postModelVar);
            console.log('Knockout init');
        },
        addPost: function(newPost) {
            //todo
            preparedPosts.push(prepare(newPost)[0]);
        },
        clearFeed: function() {
            preparedPosts = [];
        },
        digest: function() {
            observablePosts(preparedPosts);
        },
        getModuleName: function() {
            return "Knockout";
        },
        like: function(likedPost) {
            var observableUser = currentUser;
            preparedPosts[likedPost].likes.push(ko.observable(observableUser));
        },
        likeAll: function(numberOfPosts) {
            for (var i = 0; i < numberOfPosts; i++) {
                self.like(i);
            }
            //todo
        },
        setPosts: function(newPosts) {
            preparedPosts = prepare(newPosts);
        }
    };
});