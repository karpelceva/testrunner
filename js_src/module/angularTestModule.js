define(['angular'], function(angular) {
    "use strict";
    var self;

    var numberOfPosts = 0,
        likedPost = 0;

    return {
        app: angular.module('socialApp', []),
        $myScope: null,
        $filter: null,
        init: function() {
            self = this;
            self.app.config(function($interpolateProvider) {
                $interpolateProvider.startSymbol('{[').endSymbol(']}');
            });
            self.app.controller('SocialController', ['$scope', '$rootScope', '$timeout', '$filter', function($scope, $rootScope, $timeout, $filter) {
                self.$myScope = $scope;
                $scope.posts = {};

                self.$filter = $filter;

                $scope.currentUser = {
                    "_id": "5518298f90c066d2a7438442",
                    "username": "TSI user"
                };

                $('.js-posts-angular').on('click', '.js-heart-angular', function() {
                    var index = $('.js-post').index($(this).closest('.post'));
                    self.like(index);
                    self.digest();
                });
                console.log('Angular init controller');
            }]);

            $('.js-add-post').on('click', function() {
                self.addPost();
            });
            console.log('Angular init');
        },

        addPost: function(newPost) {
            self.$myScope.posts.unshift(newPost[0]);
        },
        clearFeed: function() {
            self.$myScope.posts = {};
        },
        //@todo: name in more generic way
        digest: function() {
            self.$myScope.$digest(); //call watcher functions which will trigger listeners on changed values(all)
        },
        getModuleName: function() {
            return "Angular";
        },
        like: function(likedPost) {
            var indexLike = self.$myScope.posts[likedPost].likes.indexOf(self.$myScope.currentUser);
            if (indexLike == -1) {
                self.$myScope.posts[likedPost].likes.push(self.$myScope.currentUser);
            }
        },
        likeAll: function(numberOfPosts) {
            for (var i = 0; i < numberOfPosts; i++) {
                self.like(i);
            }
        },
        setPosts: function(data) {
            self.$myScope.posts = data;
        }
    };
});
