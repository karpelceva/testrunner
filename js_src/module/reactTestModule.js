define(['react'], function(React) {
    var data = {
        posts: null,
        currentUser: {
            "_id": "5518298f90c066d2a7438442",
            "username": "TSI user"
        }
    };

    var Like = React.createClass({
        displayName: "Like",
        render: function() {
            return (
                React.createElement("div", {
                        className: "like clearfix"
                    },
                    this.props.username
                )
            );
        }
    });

    var LikeList = React.createClass({
        displayName: "LikeList",
        render: function() {

            var likeList = this.props.postLikes.map(function(like) {
                return (
                    React.createElement(Like, {
                        username: like.username,
                        key: like._id
                    })
                );
            });

            return (
                React.createElement("div", {
                        style: {
                            display: this.props.display
                        },
                        className: "likes"
                    },
                    likeList
                )
            );
        }
    });

    var Comment = React.createClass({
        displayName: "Comment",
        render: function() {
            return (
                React.createElement("div", {
                        className: "comment clearfix"
                    },
                    React.createElement("img", {
                        className: "comment__picture",
                        alt: "",
                        src: this.props.commentData.picture
                    }),
                    React.createElement("div", {
                            className: "comment__comment-info"
                        },
                        React.createElement("div", {
                            className: "comment__username"
                        }, this.props.commentData.username, " ", React.createElement("span", {
                            className: "pointer",
                            title: this.props.commentData.date
                        }, "says"), ":"),
                        React.createElement("div", {
                            className: "comment__text"
                        }, this.props.commentData.text)
                    )
                )
            );
        }
    });
    var CommentList = React.createClass({
        displayName: "CommentList",

        render: function() {
            var commentList = this.props.postComments.map(function(comment) {
                return (
                    React.createElement(Comment, {
                        commentData: comment,
                        key: comment._id
                    })
                );
            });

            return (
                React.createElement("div", {
                        style: {
                            display: this.props.display
                        },
                        className: "comments"
                    },
                    commentList
                )
            );
        }
    });

    var Friend = React.createClass({
        displayName: "Friend",
        render: function() {
            var comma = (!this.props.last) ? ", " : "";
            return (
                React.createElement("a", {
                        href: "#"
                    },
                    this.props.name, comma
                )
            );
        }
    });

    var Mood = React.createClass({
        displayName: "Mood",
        render: function() {
            var friendCount = this.props.withWho.length,
                friends = this.props.withWho.map(function(friend, i) {
                    return (
                        React.createElement(Friend, {
                            last: i == friendCount - 1,
                            name: friend.username,
                            key: friend._id
                        })
                    );
                });

            return (
                React.createElement("div", {
                    className: "post__mood-where"
                }, "Feeling ", this.props.moodType, " with ", friends, " ")
            );
        }
    });

    var Post = React.createClass({
        displayName: "Post",

        getInitialState: function() {
            return {
                displayComments: 'none',
                displayLikes: 'block',
                likes: this.props.postData.likes
            }
        },

        handleLikeDisplay: function() {
            if (this.state.displayLikes == 'none') {
                this.setState({
                    displayComments: 'none',
                    displayLikes: 'block'
                });
                return;
            }

            this.setState({
                displayLikes: 'none'
            });

        },

        handleCommentDisplay: function() {
            if (this.state.displayComments == 'none') {
                this.setState({
                    displayComments: 'block',
                    displayLikes: 'none'
                });
                return;
            }

            this.setState({
                displayComments: 'none'
            });
        },

        handleLike: function() {
            this.setState({
                likes: this.state.likes.concat([data.currentUser])
            });
        },

        render: function() {

            var likeStyle = 'post__heart pointer js-heart-react ';

            return (
                React.createElement("article", {
                        className: "post js-post clearfix"
                    },
                    React.createElement("div", {
                            className: "post__user-info"
                        },
                        React.createElement("img", {
                            className: "post__picture",
                            src: this.props.postData.picture,
                            alt: ""
                        })
                    ),

                    React.createElement("div", {
                            className: "post__post-info"
                        },
                        React.createElement("div", {
                            className: "post__username"
                        }, this.props.postData.username, " ", React.createElement("span", {
                            className: "pointer",
                            title: this.props.postData.date
                        }, "says"), ":"),
                        React.createElement("div", {
                            className: "post__text"
                        }, this.props.postData.text),

                        React.createElement(Mood, {
                            moodType: this.props.postData.mood,
                            withWho: this.props.postData.withWho
                        }),

                        React.createElement("div", {
                            className: "post__location"
                        }, "Near ", this.props.postData.where),

                        React.createElement("div", {
                            onClick: this.handleCommentDisplay,
                            className: "post__comments-link pointer js-comment"
                        }, "Comments (", this.props.postData.comments.length, ")"),
                        React.createElement("div", {
                            onClick: this.handleLikeDisplay,
                            className: "post__likes-link pointer js-like"
                        }, "Likes (", this.state.likes.length, ")"),
                        React.createElement("img", {
                            src: "img/like.png",
                            className: likeStyle
                        }),

                        React.createElement(CommentList, {
                            display: this.state.displayComments,
                            postComments: this.props.postData.comments
                        }),
                        React.createElement(LikeList, {
                            display: this.state.displayLikes,
                            postLikes: this.state.likes
                        })
                    )
                )
            );
        }

    });

    var PostList = React.createClass({
        displayName: "PostList",
        getInitialState: function() {
            return {
                posts: []
            }
        },
        updatePosts: function() {
            this.setState({
                posts: data.posts
            });
        },
        render: function() {
            var index = 0,
                postList = this.state.posts.map(function(post) {

                    var newPost = React.createElement(Post, {
                        postData: post,
                        key: post._id,

                    });
                    return React.addons.cloneWithProps(newPost, {
                        ref: 'post'+(index++)
                    });
                });

            return (
                React.createElement("main", {
                    className: "posts js-posts js-posts-react"
                }, postList)
            );
        }
    });

    var self = null;
    return {
        init: function() {
            self = this;
            self.pointerOnPosts = React.render(
                React.createElement(PostList, {
                    posts: data.posts
                }),
                document.getElementById('reactTest')
            );
            console.log('React init');
        },
        addPost: function(post) {
            data.posts.push(post[0]);
        },
        clearFeed: function() {
            data.posts = [];
        },
        digest: function() {
            self.pointerOnPosts.updatePosts();
        },
        getModuleName: function() {
            return "React";
        },
        like: function(likedPost) {
            self.pointerOnPosts.refs['post'+likedPost].handleLike();
        },
        likeAll: function(numberOfPosts) {
            for(var i = 0; i < numberOfPosts; i++) {
                data.posts[i].likes.push(data.currentUser);
            }
        },
        setPosts: function(posts) {
            data.posts = posts;
        }
    }
});
