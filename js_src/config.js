require.config({
    paths: {
        jquery: '../bower_components/jquery/dist/jquery.min',
        //KO
        knockout: '../bower_components/knockout/dist/knockout',
        knockoutTestModule: 'module/knockoutTestModule',
        //Angular
        angular: '../bower_components/angular/angular.min',
        angularTestModule: 'module/angularTestModule',
        //React
        react: '../bower_components/react/react-with-addons.min',
        reactTestModule: 'module/reactTestModule',
        //Ember
        emberTemplateCompiler: '../bower_components/ember/ember-template-compiler',
        ember: '../bower_components/ember/ember.min',
        // handlebars: '../bower_components/handlebars/handlebars',
        emberTestModule: 'module/emberTestModule',

        jsonGenerator: 'module/generator'
    },
    modules: [
        {name: "common"},
        {name: "pages/main"},
        {name: "pages/angularpage"},
        {name: "pages/reactpage"},
        {name: "pages/knockoutpage"},
        {name: "pages/emberpage"}
    ],
    shim: {
        angular: {
            exports: 'angular'
        }
    }
});