//Gruntfile
module.exports = function (grunt) {
    require('jit-grunt')(grunt);

    // Initializing the configuration object
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        appVersion: new Date().valueOf(),

        // requirejs
        'requirejs': {
            dev: {
                options: {
                    baseUrl: "./js_src/",
                    mainConfigFile: "./js_src/config.js",
                    dir: "js",
                    findNestedDependencies: true,
                    optimize: "none"
                }
            }
        },

        'clean': {
            js_folder: ['./js/*', '!./js/.*']
        },

        // minify js
        'uglify': {
            options: {
                mangle: false,
                beautify: false,
                compress: false,
                preserveComments: false,
                report: 'min',
                warnings: true
            },
            requirejs: {
                files: {
                    './js/require.min.js': './bower_components/requirejs/require.js'
                }
            },
            js_folder: {
                files: [{
                    expand: true,
                    cwd: 'js',
                    src: '**/*.js',
                    dest: 'js'
                }]
            }
        },


        // live coding use: grunt watch
        'watch': {
            js_common_folder: {
                files: [
                    './js_src/**/*.js'
                ],
                tasks: ['requirejs:dev']
            }
        }
    });

    grunt.registerTask('default', [
        'clean:js_folder',
        'requirejs:dev',
        'gitkeep:js',
        'watch'
    ]);

    grunt.registerTask('gitkeep:js', 'Creates an empty file gitkeep', function () {
        grunt.file.write('./js/.gitkeep', '');
    });
};