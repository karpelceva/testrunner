<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once 'twig/lib/Twig/Autoloader.php';

Twig_Autoloader::register();

$twig = new Twig_Environment(new Twig_Loader_Filesystem(__DIR__), array(
    'cache' => false,
    'debug' => true,
));

$twig->addFunction('path', new Twig_Function_Function('path'));
$twig->addFunction('is_active', new Twig_Function_Function('is_active'));
$twig->addFunction('navigation', new Twig_Function_Function('navigation', array(
    'needs_environment' => true,
    'is_safe' => array('html')
)));

function path($page, $user = null) {
    if ($page[0] == '/') {
        $page = substr($page, 1);
    }
    if (strlen($page) > 6 && substr($page, -6) == '/index') {
        $page = substr($page, 0, -6);
    }
    if (null === $user) {
        $user = !empty($_GET['user']) === true;
    }
    return 'index.php?page=' . $page;
}

function navigation(Twig_Environment $twig, array $items, $template = 'templates/navigation.html', $user_author = false ) {
    foreach ($items as $index => $item) {
        $path = $item['path'];

        $items[$index]['path']   = path($path);
        $items[$index]['active'] = is_active($path);
    }

    return $twig->render($template, array(
       'items' => $items,
      'user_author' => $user_author
    ));
}

function is_active($path) {
    $page   = !empty($_GET['page']) ? $_GET['page'] : 'index';
    $active = $path === $page;

    if (!$active && strlen($path) > 6 && substr($path, -6) == '/index') {
        $active = substr($path, 0, -6) === $page;
    } else if (!$active && strlen($page) >= strlen($path)) {
        $active = $path === substr($page, 0, strlen($path));
    }

    return $active;
}

$page = !empty($_GET['page']) ? $_GET['page'] : 'index';

$path = 'templates/pages/' . $page;
if (is_dir($path)) {
    $path .= '/index';
}
$path .= '.html.twig';


if (file_exists('results.txt') && isset($_GET['results'])) {
    echo file_get_contents('results.txt');
    exit;
}

echo $twig->render($path, array(
    'user' => !empty($_GET['user']) === true,
    'exists' => (file_exists('results.txt') && (file_get_contents('results.txt') != null))
));



if (!empty($_POST['results'])) {
    file_put_contents('results.txt', $_POST['results']);
}